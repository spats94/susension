function D = getD(paramsSuspension)
M_z = getM_z(paramsSuspension);
k_r = getk_r(paramsSuspension);

D = [ zeros(7,4);
     -M_z^(-1) * k_r ];
end