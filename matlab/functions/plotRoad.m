clc
plot(road_fl(:,1), road_fl(:,2), '-k');hold on; grid on;
plot(road_fr(:,1), road_fr(:,2), '--k')
plot(road_rl(:,1), road_rl(:,2), ':k')
plot(road_rr(:,1), road_rr(:,2), '-.k')
legend('Переднее левое колесо', 'Переднее правое колесо', 'Заднее левое колесо', 'Заднее правое колесо')