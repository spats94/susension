function M_z = getM_z(paramsSuspension)
M_s = getM_s(paramsSuspension);
M_u = getM_u(paramsSuspension);
M_z = [M_s         zeros(3,4);
       zeros(4,3)  M_u];
end