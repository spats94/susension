function M_s = getM_s(paramsSuspension)
% try
%     paramsSuspension.m_s;
%     paramsSuspension.J_tetta;
%     paramsSuspension.J_phi;
% catch
%     error('Недосточные данные paramsSuspension')
% end

m_s = paramsSuspension.m_s;
J_tetta = paramsSuspension.J_tetta;
J_phi = paramsSuspension.J_phi;

M_s = diag([m_s;J_tetta;J_phi]);
end