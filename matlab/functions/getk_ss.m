function k_ss = getk_ss(paramsSuspension)
% try
%     paramsSuspension.k_f;
%     paramsSuspension.k_r;
% catch
%     error('Недостаточно данных paramsSuspension(k_f, k_r)');
% end

k_f = paramsSuspension.k_f;
k_r = paramsSuspension.k_r;

k_ss = diag([k_f;k_f;k_r;k_r]);
end