%% Plotting graphs 
figure(1)
plot(out.passive_suspension.dt2_z_s, '-b'); hold on; grid on;
plot(out.active_suspension.dt2_z_s, '--r'); hold on; grid on;
legend('Пассивная система','Активная система')
xlabel('time(seconds)','Interpreter','latex')
ylabel('$\ddot{x_{s}}$','Interpreter','latex')
title('Линейное ускорение кузова')

figure(2)
plot(out.passive_suspension.dt2_theta, '-b'); hold on; grid on;
plot(out.active_suspension.dt2_theta, '--r'); hold on; grid on;
legend('passive system','active system')
xlabel('time(seconds)','Interpreter','latex')
ylabel('$\ddot{\theta}$','Interpreter','latex')
title('Угловое ускорение кузова по крену')

figure(3)
plot(out.active_suspension.dt2_theta, '-b'); hold on; grid on;
legend('active system')
xlabel('time(seconds)','Interpreter','latex')
ylabel('$\ddot{\phi}$','Interpreter','latex')
title('Угловое ускорение кузова по крену')

figure(4)
plot(out.passive_suspension.dt2_phi, '-b'); hold on; grid on;
plot(out.active_suspension.dt2_phi, '--r'); hold on; grid on;
legend('passive system','active system')
xlabel('time(seconds)','Interpreter','latex')
ylabel('$\ddot{\phi}$','Interpreter','latex')
title('Угловое ускорение кузова по тангажу')

figure(5)
plot(out.active_suspension.dt2_phi, '-b'); hold on; grid on;
legend('active system')
xlabel('time(seconds)','Interpreter','latex')
ylabel('$\ddot{\phi}$','Interpreter','latex')
title('Угловое ускорение кузова по крену')

% figure(4)
% plot(out.passive_suspension.dt_z_uf1); hold on; grid on;
% plot(out.active_suspension.dt_z_uf1);
% legend('passive system','active system')
% xlabel('time(seconds)','Interpreter','latex')
% ylabel('$\ddot{\theta}$','Interpreter','latex')
%  title('Линейное ускорение кузова')

