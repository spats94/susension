function B = getB(paramsSuspension)
M_z = getM_z(paramsSuspension);
T_a = getT_a(paramsSuspension);

B = [ zeros(7,4);
     -M_z^(-1) * T_a ];

end