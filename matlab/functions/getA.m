function A = getA(paramsSuspension)
M_z = getM_z(paramsSuspension);
k_z = getk_z(paramsSuspension);
B_z = getB_z(paramsSuspension);

A = [ zeros(7,7)        diag(ones(7,1));
     -M_z^(-1) * k_z   -M_z^(-1) * B_z    ];
end