function B_s = getB_s(paramsSuspension)
% try
%     paramsSuspension.b_f;
%     paramsSuspension.b_r;
% catch
%     error('Недостаточно данных paramsSuspension(b_f, b_r)');
% end
b_f = paramsSuspension.b_f;
b_r = paramsSuspension.b_r;

B_s = diag([b_f;b_f;b_r;b_r]);
end