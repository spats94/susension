function M_u = getM_u(paramsSuspension)
% try
%     paramsSuspension.m_f;
%     paramsSuspension.m_r;
% catch
%     error('Недосточные данные paramsSuspension')
% end

m_f = paramsSuspension.m_f;
m_r = paramsSuspension.m_r;

M_u = diag([m_f;m_f;m_r;m_r]);
end