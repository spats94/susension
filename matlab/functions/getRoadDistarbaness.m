function [road_fr, road_rr, road_fl, road_rl] = getRoadDistarbaness(...
                                                tmax, v0, ...
                                                l_right, a_right, ...
                                                l_left, a_left, ...
                                                lf, lr)
time = 0:0.001:tmax;
road_fr = zeros(length(time), 1);
road_rr = zeros(length(time), 1);
road_fl = zeros(length(time), 1);
road_rl = zeros(length(time), 1);
% The bump road disturbances right front and rear wheels
for i = 1:length(time)
    
    % The bump road disturbances right front wheels
    if time(i) <= l_right / v0
        road_fr(i) = a_right/2 * (1 - cos((2 * pi * v0 /l_right) * time(i)));
    end
    % The bump road disturbances right rear wheels
    if (time(i) <= l_right / v0 + (lf + lr) / v0) && (time(i) >= (lf + lr) / v0)
        road_rr(i) = a_right/2 * (1 - cos((2*pi*v0/l_right)*(time(i) - (lf+lr)/v0)));
    end
    
    % The bump road disturbances lef front wheels
    if time(i) <= l_left / v0
        road_fl(i) = a_left/2 * (1 - cos((2 * pi * v0 /l_left) * time(i)));
    end
    % The bump road disturbances lef rear wheels
    if (time(i) <= l_left / v0 + (lf + lr) / v0) && (time(i) >= (lf + lr) / v0)
        road_rl(i) = a_left/2 * (1 - cos((2*pi*v0/l_left)*(time(i) - (lf+lr)/v0)));
    end
end
road_fr = [time' road_fr];
road_rr = [time' road_rr];
road_fl = [time' road_fl];
road_rl = [time' road_rl];