function k_z = getk_z(paramsSuspension)
T = getT(paramsSuspension);
k_sr = getk_sr(paramsSuspension);
k_ss = getk_ss(paramsSuspension);
k_t = getk_t(paramsSuspension);

k_z = [ T * k_sr * T'   -T * k_sr;
       -k_ss * T'        k_t + k_ss];
end








