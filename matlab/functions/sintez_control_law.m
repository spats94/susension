%%
paramsSuspension = getParamsSuspension()
A = getA(paramsSuspension);
C1 = A(7:9, :);
B1 = getB(paramsSuspension);
B2 = B1(7:9, :);
D = getD(paramsSuspension);

 
%%
n1 = 14
step = 0.1;
begin_val = 19;%0.1;%
end_val = 21;%
min_tr_Z = 1000000000000;
% Optimization of parameter q by searching with decreasing lead
while step>=0.0001
    for q = begin_val:step:end_val
        cvx_begin sdp
        variable Qs(n1, n1) symmetric;
        variable Zs(4,4) symmetric;
        variable Ys(4, n1) ;
        variable bet ;
        minimize( trace(C1*Qs*C1'+C1*Ys'*B2'+B2*Ys*C1'+B2*Zs*B2'))
        subject to
            Qs >= 0;
            [A*Qs + Qs*A'+B1*Ys+Ys'*B1'+q*Qs D;
            D' -q*eye(4)] <= 0;
            [Zs Ys;
            Ys' Qs] >= 0;
        cvx_end
        
        Qsf = double(Qs);
        Y=double(Ys);
        K=Y/Qsf;
        Z=double(Zs);
        trZ=trace(C1*Qsf*C1'+C1*Y'*B2'+B2*Y*C1'+B2*Z*B2');
        if min_tr_Z > trZ
            min_tr_Z = trZ;
            Q_min = Qsf;
            K_min=K;
            q_min = q;
        end
    end
    step = step*0.5;
    begin_val = q_min-2*step;
    end_val = q_min+2*step;
end
Q1 = Q_min
K1 = K_min
q1 = q_min

