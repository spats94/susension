function B_z = getB_z(paramsSuspension)
T = getT(paramsSuspension);
B_s = getB_s(paramsSuspension);

B_z = [ T*B_s*T'  -T*B_s;
       -B_s*T'     B_s];
end