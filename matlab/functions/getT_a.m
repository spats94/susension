function T_a = getT_a(paramsSuspension)
T = getT(paramsSuspension);

T_a = [ T;
        zeros(4,4)];
end