function k_sr = getk_sr(paramsSuspension)
% try
%     paramsSuspension.k_f;
%     paramsSuspension.k_r;
%     paramsSuspension.r_f;
%     paramsSuspension.r_r;
% catch
%     error('Недостаточно данных paramsSuspension(k_f, k_r, r_f, r_r)');
% end

k_f = paramsSuspension.k_f;
k_r = paramsSuspension.k_r;
r_f = paramsSuspension.r_f;
r_r = paramsSuspension.r_r;

k_sr = [ k_f + r_f/2  -r_f/2         0              0;
        -r_f/2         k_f + r_f/2   0              0;
         0             0             k_r + r_r/2   -r_r/2;
         0             0            -r_r/2          k_r + r_r/2];
end