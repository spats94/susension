function T = getT(paramsSuspension)
% try
%     paramsSuspension.t_f;
%     paramsSuspension.t_r;
%     paramsSuspension.l_f;
%     paramsSuspension.l_r;
% catch
%     error('Недостаточно данных paramsSuspension(t_f, t_r, l_f, l_r)');
% end

t_f = paramsSuspension.t_f;
t_r = paramsSuspension.t_r;
l_f = paramsSuspension.l_f;
l_r = paramsSuspension.l_r;

T = [1     1     1    1;
    -t_f   t_f  -t_r  t_r;
    -l_f  -l_f   l_r  l_r];
end