function k_r = getk_r(paramsSuspension)
k_t = getk_t(paramsSuspension);
k_r = [ zeros(3,4);
        k_t];
end