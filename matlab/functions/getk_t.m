function k_t = getk_t(paramsSuspension)
% try
%     paramsSuspension.k_tf;
%     paramsSuspension.k_tr;
% catch
%     error('Недостаточно данных paramsSuspension(k_tf, k_tr)');
% end

k_tf = paramsSuspension.k_tf;
k_tr = paramsSuspension.k_tr;

k_t = diag([k_tf;k_tf;k_tr;k_tr]);
end